#### wu-acw-client 使用
    项目介绍，使用acw-client，创建对应Java项目的增删改查（ORM：Lazy ORM、mybatis），项目模块架构：mvc、feign、ddd
    演示项目

    环境：idea 、mac、mysql、jdk17 spring boot 3.0.7

#### 第一步通过idea 创建一个项目

![create_project.png](create_project.png)

### 选择通过spring Initializr 创建项目 本文按照maven进行演示
![config_project.png](config_project.png)

### 创建后项目结构
![project.png](project.png)

### 使用框架管理依赖版本
```xml
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>top.wu2020</groupId>
                <artifactId>wu-framework-dependencies</artifactId>
                <version>1.2.3-JDK17</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
```
###  安装web 容器
```xml
        <!--    web 容器    -->
        <dependency>
            <groupId>top.wu2020</groupId>
            <artifactId>wu-framework-web</artifactId>
        </dependency>
```
### 安装acw-client
```xml
        <dependency>
            <groupId>top.wu2020</groupId>
            <artifactId>wu-smart-acw-client</artifactId>
            <version>1.2.3-JDK17</version>
        </dependency>
```
### 数据库选择使用mysql 添加mysql驱动
```xml
        <!--        mysql    -->
        <dependency>
            <groupId>mysql</groupId>
            <artifactId>mysql-connector-java</artifactId>
            <version>8.0.33</version>
        </dependency>
```
### ORM 选择安装(mybatis/Lazy ORM)
```xml
        <!--    ORM mybatis    -->
        <dependency>
            <groupId>com.baomidou</groupId>
            <artifactId>mybatis-plus-boot-starter</artifactId>
            <version>3.5.4</version>
        </dependency>
        <!--    ORM Lazy ORM    -->
        <dependency>
            <groupId>top.wu2020</groupId>
            <artifactId>wu-database-lazy-starter</artifactId>
        </dependency>
```

### 项目启动

![start_project.png](start_project.png)

### 使用

    打开控制台url地址访问页面

![open_url.png](open_url.png)

### 本地项目自动生成对应代码

![success.png](success.png)

[Lazy ORM 框架地址](https://gitee.com/wujiawei1207537021/wu-framework-parent/tree/master/wu-inner-intergration/wu-database-parent)
[Acw 客户端地址](https://gitee.com/wujiawei1207537021/wu-framework-parent/tree/master/wu-smart-intergration/wu-smart-acw/wu-smart-acw-client)

[演示项目地址](https://gitee.com/wujiawei1207537021/wu-acw-client-simple)