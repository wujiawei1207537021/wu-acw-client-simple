package wu2020.top.wu.acw.client.simple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WuAcwClientSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(WuAcwClientSimpleApplication.class, args);
    }

}
